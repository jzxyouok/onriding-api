package com.cloudwave.trailends.entity;

import java.io.Serializable;
import java.util.List;

/**
 * 上传数据时，用户接受服务器返回值的实体类
 * @author 3kqing
 *
 */
public class SyncIdEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public long id;
	public long remoteId;
	
	public List<SyncIdEntity> idList;
	
}
