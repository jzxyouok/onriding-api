package com.cloudwave.trailends.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudwave.base.action.AppAction;

/**
 * @description 通用业务处理
 * @author DolphinBoy
 * @date 2013年12月10日 下午2:24:02
 * TODO
 */

@Controller
@RequestMapping("/api")
public class IndexAction extends AppAction {
	private static Logger logger = LoggerFactory.getLogger(IndexAction.class);

	
	@RequestMapping(method=RequestMethod.GET, value="/index")
	public @ResponseBody String index() {
		
		logger.info("获取系统信息.");
		return "非法访问!";
	}
}
