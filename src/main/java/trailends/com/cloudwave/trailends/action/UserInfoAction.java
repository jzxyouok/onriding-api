package com.cloudwave.trailends.action;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.cloudwave.base.action.AppAction;
import com.cloudwave.base.entity.ResultEntity;
import com.cloudwave.fwcore.entity.Result;
import com.cloudwave.trailends.domain.UserInfo;
import com.cloudwave.trailends.service.UserInfoService;

/**
 * @description 
 * @author 龙雪
 * @date 2014年12月24日
 * 
 */

@Controller
@RequestMapping("/api/userinfo")
public class UserInfoAction extends AppAction {
	private static Logger logger = LoggerFactory.getLogger(UserInfoAction.class);
	
	@Resource
	private UserInfoService userInfoService;
	
	
	@RequestMapping(method=RequestMethod.POST, value="/update")
	public @ResponseBody ResultEntity update(@RequestParam(value="data") String data) {
		UserInfo userInfo = null;
		try {
			userInfo = JSON.parseObject(data, UserInfo.class);
		} catch (Exception e) {
			logger.error("user info parse error!", e);
			return resultEntity.failure(Result.PARSE_FAILURE);
		}
		
		if ( userInfo != null ) {
			if ( userInfo.getUserId() == null
					|| userInfo.getUserId() == 0 ) {
				userInfo.setUserId(super.userId);
			}
		} else {
			return resultEntity.failure(Result.ILLEGAL_REQUEST_PARAMS);
		}
		
		try {
			userInfoService.update(userInfo);
			return resultEntity.success();
		} catch (Exception e) {
			logger.error("user info update error!", e);
			return resultEntity.failure(Result.UPDATE_FAILURE);
		}
	}
	
	
	
	
	
}
