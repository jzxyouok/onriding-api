package com.cloudwave.trailends.action;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.cloudwave.base.action.AppAction;
import com.cloudwave.base.entity.ResultEntity;
import com.cloudwave.fwcore.entity.Result;
import com.cloudwave.fwcore.utils.DateUtils;
import com.cloudwave.trailends.domain.Feedback;
import com.cloudwave.trailends.service.FeedbackService;

/**
 * @description 
 * @author wangwenlong
 * 2013年12月6日 下午3:06:52
 * TODO
 */

@Controller
@RequestMapping("/api/feedback")
public class FeedbackAction extends AppAction {
	private static Logger logger = LoggerFactory.getLogger(FeedbackAction.class);
	
	@Resource
	private FeedbackService feedbackService;
	
	@RequestMapping(method=RequestMethod.POST, value="/add")
	public @ResponseBody ResultEntity add(@RequestParam(value="entity") String entity) {
		Feedback fb = null;
		try {
			fb = JSON.parseObject(entity, Feedback.class);
		} catch (Exception e) {
			logger.error("解析反馈对象时异常：", e);
			return resultEntity.failure(Result.PARSE_FAILURE);
		}
		
		if ( fb == null ) {
			return resultEntity.failure(Result.ILLEGAL_PARAM);
		}
		
		fb.setProduct("onriding_android");
		fb.setUpdateTime(DateUtils.now());
		fb.setCreatedTime(DateUtils.now());
		
		feedbackService.save(fb);
		
		return resultEntity.success();
	}
	
}
