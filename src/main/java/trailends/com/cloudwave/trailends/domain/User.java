package com.cloudwave.trailends.domain;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.cloudwave.base.domain.BaseEntity;
import com.cloudwave.trailends.AppProerties;
import com.cloudwave.trailends.entity.UserInfoEntity;

/**
 * User: 龙雪
 * Date: 13-7-6
 * Time: 下午9:56
 * 用户 实体类
 */
public class User extends BaseEntity {
	private static final long serialVersionUID = 6540471194357488833L;
	
	private String uid;
//	private String account;
	private String username;
	private String nickname;
	private String password;
	private String email;
	private String mobile;
	private String avatar;
	private int gender;
	
	private String intro;
	private String city;
	private String address;
	
	private Integer age;
	private Integer height;
	private Float weight;
	
	private Date regtime;
	private int status = 1;
	
	private String signature;
	private String token;
	private String accessToken;
	private String openid;
	
	private float totalMileage;
	private long totalRidingTime;
	private int totalRidingLine;
	private long totalLinePoint;
	
	private int honor;  //荣誉值
	
	private int favorites;  //收藏
	
	private int notices;
	private int replies;
	
	private int follows;
	private int followers;
	
	public User() {
		super();
	}
	public User(long id) {
		super();
		this.id = id;
	}
	
	public User(UserInfoEntity userInfo) {
		this.setId(userInfo.getUserId());
		this.setGender(userInfo.getGender());
		this.setHeight(userInfo.getHeight());
		this.setWeight(userInfo.getWeight());
		this.setCity(userInfo.getCity());
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
//	public String getAccount() {
//		return account;
//	}
//	public void setAccount(String account) {
//		this.account = account;
//	}
	public String getUsername() {
		if ( StringUtils.isEmpty(username) ) {
			return nickname;
		}
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAvatar() {  //不返回默认头像了
		if (StringUtils.isNotEmpty(avatar)) {
			if (avatar.startsWith("http")) {
				return avatar;
			} else {
				return AppProerties.HOST_STATIC + avatar;
			}
		} else {
			return avatar;
		}
		
		
//		if (avatarUrl == null || avatarUrl.length() == 0) {
//			return AppProerties.DEFAULT_AVATAR_URL;
//		} else {
//			if (avatarUrl.startsWith("http")) {
//				return avatarUrl;
//			} else {
//				return AppProerties.HOST + avatarUrl;
//			}
//			
//		}
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getNotices() {
		return notices;
	}
	public void setNotices(int notices) {
		this.notices = notices;
	}
	public int getReplies() {
		return replies;
	}
	public void setReplies(int replies) {
		this.replies = replies;
	}
	public int getFollows() {
		return follows;
	}
	public void setFollows(int follows) {
		this.follows = follows;
	}
	public int getFollowers() {
		return followers;
	}
	public void setFollowers(int followers) {
		this.followers = followers;
	}
	public int getHonor() {
		return honor;
	}
	public void setHonor(int honor) {
		this.honor = honor;
	}
	public int getFavorites() {
		return favorites;
	}
	public void setFavorites(int favorites) {
		this.favorites = favorites;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public float getTotalMileage() {
		return totalMileage;
	}
	public void setTotalMileage(float totalMileage) {
		this.totalMileage = totalMileage;
	}
	public int getTotalRidingLine() {
		return totalRidingLine;
	}
	public void setTotalRidingLine(int totalRidingLine) {
		this.totalRidingLine = totalRidingLine;
	}
	public long getTotalLinePoint() {
		return totalLinePoint;
	}
	public void setTotalLinePoint(long totalLinePoint) {
		this.totalLinePoint = totalLinePoint;
	}
	public long getTotalRidingTime() {
		return totalRidingTime;
	}
	public void setTotalRidingTime(long totalRidingTime) {
		this.totalRidingTime = totalRidingTime;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public int getGender() {
		return gender;
	}
	public String getGenderV() {
		switch (gender) {
		case 0:
			return "未知";
		case 1:
			return "男";
		case 2:
			return "女";
		case -1:
			return "其他";
		default:
			return "未知";
		}
	}
	public void setGender(String gender) {
		try {
			this.gender = Integer.parseInt(gender);
		} catch (Exception e) {
			this.gender = -1;
		}
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	public String getIntro() {
		return intro;
	}
	public void setIntro(String intro) {
		this.intro = intro;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Integer getHeight() {
		return height;
	}
	public void setHeight(Integer height) {
		this.height = height;
	}
	public Float getWeight() {
		return weight;
	}
	public void setWeight(Float weight) {
		this.weight = weight;
	}
//	@JsonSerialize(using=CustomDateSerializer.class)
	public Date getRegtime() {
		return regtime;
	}
	public void setRegtime(Date regtime) {
		this.regtime = regtime;
	}
	
}
