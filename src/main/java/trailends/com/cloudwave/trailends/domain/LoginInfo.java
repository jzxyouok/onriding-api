package com.cloudwave.trailends.domain;

import java.util.Date;

import com.cloudwave.base.domain.BaseEntity;

/**
 * @description 登录信息 实体类
 * @author DolphinBoy
 * @date 2013年12月26日
 * TODO
 */

public class LoginInfo extends BaseEntity {
	private static final long serialVersionUID = 1L;

	private Long userId;
	private String token;
	private Date loginTime;
	
	private User user;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}