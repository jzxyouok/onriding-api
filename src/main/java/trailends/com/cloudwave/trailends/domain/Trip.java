package com.cloudwave.trailends.domain;

import java.util.Date;
import java.util.List;

import com.cloudwave.base.domain.AppDomain;

/**
 * @description 行记 实体类
 * @author DolphinBoy
 * @date 2013年12月10日 下午4:16:04
 * TODO
 */

public class Trip extends AppDomain {
	private static final long serialVersionUID = 1L;

	private String title;
	private Date beginDate;
	private Date endDate;
	
	private Location beginLocation;
	private Location endLocation;
	
	private User user;
	
	private List<TripRecord> trList;
	private List<User> partner;
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Location getBeginLocation() {
		return beginLocation;
	}
	public void setBeginLocation(Location beginLocation) {
		this.beginLocation = beginLocation;
	}
	public Location getEndLocation() {
		return endLocation;
	}
	public void setEndLocation(Location endLocation) {
		this.endLocation = endLocation;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<TripRecord> getTrList() {
		return trList;
	}
	public void setTrList(List<TripRecord> trList) {
		this.trList = trList;
	}
	public List<User> getPartner() {
		return partner;
	}
	public void setPartner(List<User> partner) {
		this.partner = partner;
	}
}
