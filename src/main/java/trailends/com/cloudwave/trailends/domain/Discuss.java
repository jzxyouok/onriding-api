package com.cloudwave.trailends.domain;

import com.cloudwave.base.domain.AppDomain;


/**
 * 评论  实体类
 * @author DolphinBoy
 * @date 2013-8-23
 */
public class Discuss extends AppDomain {
	private static final long serialVersionUID = -4625028862529199355L;
	
	private TripRecord travelMessage;
	private String content;
	
	
	public TripRecord getTravelMessage() {
		return travelMessage;
	}
	public void setTravelMessage(TripRecord travelMessage) {
		this.travelMessage = travelMessage;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
}
