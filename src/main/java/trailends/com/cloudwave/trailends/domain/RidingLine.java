package com.cloudwave.trailends.domain;

import java.util.Date;
import java.util.List;

import com.cloudwave.base.domain.AppDomain;
import com.cloudwave.fwcore.utils.DateUtils;


/**
 * @description 
 * @author 龙雪
 * @date 2014年7月26日
 * TODO
 */

public class RidingLine extends AppDomain {
	private static final long serialVersionUID = 1L;
	public static final int HASNOT_BEGUN = 0;
	public static final int HAS_BEGUN = 1;
	public static final int HAS_END = 2;
	
	public static final int IS_PUBLIC_NO = 0;
	public static final int IS_PUBLIC_YES = 1;
	
	public RidingLine() { }
	public RidingLine(Long id) {
		this.id = id;
	}
	public RidingLine(String title) {
		this.title = title;
	}
	
	private String serialNumber;
	private String title;
//	private String ridingDate;
	private Date startTime;
	private Date endTime;
	private int times;
	private String description;
	
	private Float userWeight;  //当时用户体重
	
	private float totalMileage = 0.00f;  //总距离
	private int totalPoint = 0;  //坐标点总数
	private long totalTime = 0;  //时长
	private long totalValidTime = 0;  //有效时长
	
	private Float totalRise;  //累计上升
	private Float totalDrop;  //累计下降
//	private Float altitudeRange;  //海拔范围
	private Double maxAltitude;  //最大海拔
	private Double minAltitude;  //最小海拔
	private Float totalClimb; //爬升
	private Float totaDecline;  //下降
	private Float totalUpSlope;  //上坡坡程
	private Float totalDownSlope;  //下坡陂程
	
	private Float maxSpeed = 0.00f;  //最大速度
	private float avgSpeed = 0.00f;  //平均速度
	private Float maxGpsSpeed;  //GPS最大速度
	private Float avgGpsSpeed;  //GPS平均速度
	private Float avgAltitude = 0.00f;  //平均海拔
	private Float kcal = 0.00f;  //消耗卡路里
	
	private String startAddr;  //出发位置
	private String endAddr;  //结束位置
	
	private int score;  //得分
	
	private int isPublic = IS_PUBLIC_YES;  //行记的状态
	private int isSyncWeibo;  //是否同步到新浪微博
	private int isSyncWblog;  //是否同步到腾讯微博
	private int isSyncQQZone;  //是否同步到腾讯微博
	
	private int commentCount = 0;  //评论数
	private int zanCount = 0;  //赞数
	
	private int status = HAS_BEGUN;  //行记的状态
	
	private String model;
	
	private List<LinePoint> lpList;  //路径数据
//	private List<User> partners;  //参与者

	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public int getTimes() {
		return times;
	}
	public void setTimes(int times) {
		this.times = times;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Float getUserWeight() {
		return userWeight;
	}
	public void setUserWeight(Float userWeight) {
		this.userWeight = userWeight;
	}
	public float getTotalMileage() {
		return totalMileage;
	}
	public String getTotalMileageV() {
		return String.format("%.2f", totalMileage / 1000);
	}
	public void setTotalMileage(float totalMileage) {
		this.totalMileage = totalMileage;
	}
	public int getTotalPoint() {
		return totalPoint;
	}
	public void setTotalPoint(int totalPoint) {
		this.totalPoint = totalPoint;
	}
	public long getTotalTime() {
		return totalTime;
	}
	public String getTotalTimeV() {
		if ( totalTime != 0 ) {
			int[] date = DateUtils.duration(totalTime * 1000);
			return date[1]+"小时"+date[2]+"分"+date[3]+"秒";
		}
		return "0";
	}
	public void setTotalTime(long totalTime) {
		this.totalTime = totalTime;
	}
	public long getTotalValidTime() {
		return totalValidTime;
	}
	public String getTotalValidTimeV() {
		if ( totalValidTime != 0 ) {
			int[] date = DateUtils.duration(totalValidTime * 1000);
			return date[1]+"小时"+date[2]+"分"+date[3]+"秒";
		}
		return "0";
	}
	public void setTotalValidTime(long totalValidTime) {
		this.totalValidTime = totalValidTime;
	}
	public Float getTotalRise() {
		return totalRise;
	}
	public void setTotalRise(Float totalRise) {
		this.totalRise = totalRise;
	}
	public Float getTotalDrop() {
		return totalDrop;
	}
	public void setTotalDrop(Float totalDrop) {
		this.totalDrop = totalDrop;
	}
	public String getAltitudeRange() {
		if ( maxAltitude != null && minAltitude != null ) {
			return maxAltitude+"~"+minAltitude;
		} else {
			return "--";
		}
	}
	public Double getMaxAltitude() {
		return maxAltitude;
	}
	public void setMaxAltitude(Double maxAltitude) {
		this.maxAltitude = maxAltitude;
	}
	public Double getMinAltitude() {
		return minAltitude;
	}
	public void setMinAltitude(Double minAltitude) {
		this.minAltitude = minAltitude;
	}
	public Float getTotalClimb() {
		return totalClimb;
	}
	public void setTotalClimb(Float totalClimb) {
		this.totalClimb = totalClimb;
	}
	public Float getTotaDecline() {
		return totaDecline;
	}
	public void setTotaDecline(Float totaDecline) {
		this.totaDecline = totaDecline;
	}
	public Float getTotalUpSlope() {
		return totalUpSlope;
	}
	public void setTotalUpSlope(Float totalUpSlope) {
		this.totalUpSlope = totalUpSlope;
	}
	public Float getTotalDownSlope() {
		return totalDownSlope;
	}
	public void setTotalDownSlope(Float totalDownSlope) {
		this.totalDownSlope = totalDownSlope;
	}
	public Float getMaxSpeed() {
		return maxSpeed;
	}
	public void setMaxSpeed(Float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public float getAvgSpeed() {
		if (totalTime > 0) {
			float km = totalMileage / (float) 1000;
			float hour = (float) totalTime / (float) (60 * 60);
			avgSpeed = km / hour;
			return avgSpeed;
		} else {
			return avgSpeed;
		}
	}
	public float getValidAvgSpeed() {
		if (totalTime > 0) {
			float km = totalMileage / (float) 1000;
			float hour = (float) totalValidTime / (float) (60 * 60);
			avgSpeed = km / hour;
			return avgSpeed;
		} else {
			return avgSpeed;
		}
	}
	public void setAvgSpeed(float avgSpeed) {
		this.avgSpeed = avgSpeed;
	}
	public Float getMaxGpsSpeed() {
		return maxGpsSpeed;
	}
	public void setMaxGpsSpeed(Float maxGpsSpeed) {
		this.maxGpsSpeed = maxGpsSpeed;
	}
	public Float getAvgGpsSpeed() {
		return avgGpsSpeed;
	}
	public void setAvgGpsSpeed(Float avgGpsSpeed) {
		this.avgGpsSpeed = avgGpsSpeed;
	}
	public Float getAvgAltitude() {
		return avgAltitude;
	}
	public void setAvgAltitude(Float avgAltitude) {
		this.avgAltitude = avgAltitude;
	}
	/**卡路里(kcal)消耗=骑车时速(km/h)×体重(kg)×9.7×运动时间(h) */
	public Float getKcal() {
		if ( userWeight != null ) {
			float hour = (float) totalTime / (float) (60 * 60);
			kcal = getAvgSpeed() * (float) userWeight * (float) 9.7 * hour;
			return kcal;
		} else {
			return null;
		}
	}
	public void setKcal(Float kcal) {
		this.kcal = kcal;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getIsPublic() {
		return isPublic;
	}
	public void setIsPublic(int isPublic) {
		this.isPublic = isPublic;
	}
	public int getIsSyncWeibo() {
		return isSyncWeibo;
	}
	public void setIsSyncWeibo(int isSyncWeibo) {
		this.isSyncWeibo = isSyncWeibo;
	}
	public int getIsSyncWblog() {
		return isSyncWblog;
	}
	public void setIsSyncWblog(int isSyncWblog) {
		this.isSyncWblog = isSyncWblog;
	}
	public int getIsSyncQQZone() {
		return isSyncQQZone;
	}
	public void setIsSyncQQZone(int isSyncQQZone) {
		this.isSyncQQZone = isSyncQQZone;
	}
	public int getCommentCount() {
		return commentCount;
	}
	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}
	public int getZanCount() {
		return zanCount;
	}
	public void setZanCount(int zanCount) {
		this.zanCount = zanCount;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public List<LinePoint> getLpList() {
		return lpList;
	}
	public void setLpList(List<LinePoint> lpList) {
		this.lpList = lpList;
	}
//	public List<User> getPartners() {
//		return partners;
//	}
//	public void setPartners(List<User> partners) {
//		this.partners = partners;
//	}
	public String getStartAddr() {
		return startAddr;
	}
	public void setStartAddr(String startAddr) {
		this.startAddr = startAddr;
	}
	public String getEndAddr() {
		return endAddr;
	}
	public void setEndAddr(String endAddr) {
		this.endAddr = endAddr;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	
}
