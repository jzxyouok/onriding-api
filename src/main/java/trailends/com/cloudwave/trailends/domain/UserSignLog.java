package com.cloudwave.trailends.domain;

import java.util.Date;

import com.cloudwave.base.domain.BaseEntity;

/**
 * 用户登录, 注册日志 实体类 
 * @author 龙雪
 * @email 569141948@qq.com
 * @date 2014年12月15日
 */

public class UserSignLog extends BaseEntity {
	private static final long serialVersionUID = 1L;
	
	public static final int ACTION_SIGNIN = 601;
	public static final int ACTION_SIGNUP = 602;
	/**SSO登录*/
	public static final int ACTION_SIGNIN_SSO = 603;
	/**SSO注册*/
	public static final int ACTION_SIGNUP_SSO = 604;
	public static final int ACTION_SYNC_DELETE = 701;
	public static final int ACTION_SYNC_DOWNLOAD_COUNT = 702;
	public static final int ACTION_SYNC_DOWNLOAD = 703;
	public static final int ACTION_SYNC_UPLOAD = 704;
	
	public static final int ACTION_RIDINGLINE_UPDATE = 1001;
	
	
	
	private Long userId;
	private User user;
	private String cid;
	private String udid;
	private String imei;
	private String phone;
	private String ip;
	private String city;
	private String platform;
	private int action;
	private Date createTime;
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public int getAction() {
		return action;
	}
	public void setAction(int action) {
		this.action = action;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getUdid() {
		return udid;
	}
	public void setUdid(String udid) {
		this.udid = udid;
	}
	
}
