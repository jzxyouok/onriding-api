package com.cloudwave.trailends.domain;

import com.cloudwave.base.domain.BaseEntity;
import com.cloudwave.trailends.emuns.ClientType;

/**
 * @description 客户端信息 实体类
 * @author DolphinBoy
 * @date 2013年12月18日 
 * @time 下午5:03:39 
 * TODO
 */

public class ClientInfo extends BaseEntity {
	private static final long serialVersionUID = -1964611917783690543L;
	
	private ClientType type;
	private String name;
	private String version;
	private User user;
	
	public ClientType getType() {
		return type;
	}

	public void setType(ClientType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
