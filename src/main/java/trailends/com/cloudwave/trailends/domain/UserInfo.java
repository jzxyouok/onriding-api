package com.cloudwave.trailends.domain;

import com.cloudwave.base.domain.BaseEntity;

public class UserInfo extends BaseEntity {
	private static final long serialVersionUID = 1L;
	
	private Long userId;
	private Integer age;
	/**1:男;2:女;-1:其他;3:未知*/
	private int gender;
	private Integer height;
	private Float weight;
	private String city;
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	public Integer getHeight() {
		return height;
	}
	public void setHeight(Integer height) {
		this.height = height;
	}
	public Float getWeight() {
		return weight;
	}
	public void setWeight(Float weight) {
		this.weight = weight;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
}