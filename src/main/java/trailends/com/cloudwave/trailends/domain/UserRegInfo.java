package com.cloudwave.trailends.domain;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.cloudwave.base.domain.BaseEntity;

/**
 * @author 龙雪
 * @email 569141948@qq.com
 * @date 2015-2-10 下午8:37:46
 * 
 */

public class UserRegInfo extends BaseEntity {
	private static final long serialVersionUID = 1L;

	public static final String CLIENT_MOBILE = "mobile";
	public static final String PLATFORM_QQ = "qq";
	public static final String PLATFORM_WEIBO = "weibo";
	public static final String PLATFORM_WEIXIN = "weixin";
	
	private Long uid;
	private String cid;
	private String udid;
	private String imei;
	private String model;
	private String client = CLIENT_MOBILE;
	private String platform;
	private Date createTime;
	
	public UserRegInfo() { }
	
	public UserRegInfo(HttpServletRequest request) {
		this.cid = request.getParameter("cid");
		this.udid = request.getParameter("udid");
		this.imei = request.getParameter("imei");
		this.model = request.getParameter("model");
		this.createTime = new Date();
	}
	public Long getUid() {
		return uid;
	}
	public void setUid(Long uid) {
		this.uid = uid;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getUdid() {
		return udid;
	}
	public void setUdid(String udid) {
		this.udid = udid;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	
}
