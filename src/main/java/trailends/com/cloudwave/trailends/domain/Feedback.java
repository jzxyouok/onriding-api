package com.cloudwave.trailends.domain;

import java.util.Date;

import com.cloudwave.base.domain.BaseEntity;

/**
 * 
 * @author wangwl
 * 2015-3-17
 */

public class Feedback extends BaseEntity {
	private static final long serialVersionUID = 1L;
	
	private String title;
	private String content;
	private String qq;
	private String email;
	private String mobile;
	private String model;
	private String appVer;
	private String releaseVer;
	private String product;
	private int type;
	private int level;
	private int focusWeight;
	private int status;
	private int viewCount;
	private int responseCount;
	private int userId;
	private Date updateTime;
	private Date createdTime;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getAppVer() {
		return appVer;
	}
	public void setAppVer(String appVer) {
		this.appVer = appVer;
	}
	public String getReleaseVer() {
		return releaseVer;
	}
	public void setReleaseVer(String releaseVer) {
		this.releaseVer = releaseVer;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getFocusWeight() {
		return focusWeight;
	}
	public void setFocusWeight(int focusWeight) {
		this.focusWeight = focusWeight;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getViewCount() {
		return viewCount;
	}
	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}
	public int getResponseCount() {
		return responseCount;
	}
	public void setResponseCount(int responseCount) {
		this.responseCount = responseCount;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	
}
