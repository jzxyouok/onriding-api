package com.cloudwave.trailends.service;

import com.cloudwave.trailends.domain.UserInfo;

/**
 * @description 
 * @author wangwenlong
 * @date 2014年1月4日
 * TODO
 */

public interface UserInfoService {

	void save(UserInfo userInfo);

	UserInfo findByUserId(Long id);

	void update(UserInfo userInfo);

}
