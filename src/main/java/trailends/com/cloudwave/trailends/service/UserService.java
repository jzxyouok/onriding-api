package com.cloudwave.trailends.service;

import java.util.List;

import com.cloudwave.trailends.beans.UserHomeBean;
import com.cloudwave.trailends.domain.User;
import com.cloudwave.trailends.entity.UserInfoEntity;

/**
 * @description 
 * @author wangwenlong
 * @date 2013年12月23日
 * TODO
 */

public interface UserService {

	public User get(long id);

	public void save(User u) throws Exception;

	public User findByUserName(String username);

	public User getByAccountOrEmail(String accountOrEmail);

	public boolean checkEmail(String email);

	public boolean checkUsername(String username);

	public List<User> findByIds(List<Long> ids);

	public UserHomeBean loadUserHome(Long id);

	public User getByEmail(String email);

	public boolean checkNameAndEmail(String username, String email);

	public boolean isExistsAccount(String account);

	public void updateAvatar(Long userId, String url);

	public User findByToken(String token);
	
	public Long findIdByToken(String token);
	
	public void updateToken(Long userId, String token);
	
	/**
	 * 更新用户统计数据
	 * @param userId
	 */
	public void updateUserStatData(Long userId);

	public User findByOpenId(String openid);

	public void saveOpenUser(User u);

	public void update(UserInfoEntity userInfo);


}