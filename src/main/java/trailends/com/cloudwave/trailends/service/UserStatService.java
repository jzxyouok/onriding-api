package com.cloudwave.trailends.service;

import com.cloudwave.trailends.domain.UserRegInfo;

/**
 * @author 龙雪
 * @email 569141948@qq.com
 * @date 2015-2-10 下午8:11:06
 * 
 */

public interface UserStatService {

	void saveUserRegInfo(UserRegInfo regInfo);

}
