package com.cloudwave.trailends.service;

import java.util.List;

import com.cloudwave.base.entity.QueryEntity;
import com.cloudwave.trailends.domain.LinePoint;

/**
 * @description 
 * @author wangwenlong
 * @date 2014年7月26日
 * TODO
 */

public interface LinePointService {

	void saveAll(List<LinePoint> lpList);

	List<LinePoint> findNotInIds(String lpIds);

	Long countByLastTime(Long userId, String rlIdStr, String lastTimestamp);

	List<LinePoint> findBy(QueryEntity query);

	void save(LinePoint lp);

}
