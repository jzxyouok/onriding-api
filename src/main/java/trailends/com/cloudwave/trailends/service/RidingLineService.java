package com.cloudwave.trailends.service;

import java.util.List;
import java.util.Map;

import com.cloudwave.trailends.domain.LinePoint;
import com.cloudwave.trailends.domain.RidingLine;

/**
 * @description 
 * @author wangwenlong
 * @date 2014年7月26日
 * TODO
 */

public interface RidingLineService {

	void save(RidingLine ridingLine);

	List<RidingLine> findNotInIds(String rlIds);

	List<RidingLine> findBy(Map<String, Object> params);

	List<RidingLine> findAll(Map<String, Object> params);

	void saveLinesAndPoints(List<RidingLine> rList);

	void saveLineAndPoints(RidingLine ridingLine, List<LinePoint> lpList);

	void saveWithsPoints(RidingLine rl);

	RidingLine findById(Long id);

	void deleteById(Long id);

	void deleteByIds(List<Long> idList);

	RidingLine findByLpId(Long lpId);

	RidingLine findFirst();

	RidingLine findNextById(Long rlId);

	void update(RidingLine rlTmp);

}
