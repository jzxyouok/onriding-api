package com.cloudwave.trailends.service;

import com.cloudwave.trailends.domain.Feedback;

/**
 * @author 龙雪
 * @email 569141948@qq.com
 * @date 2015-3-17 下午9:30:42
 * 
 */

public interface FeedbackService {

	void save(Feedback fb);

}
