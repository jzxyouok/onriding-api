package com.cloudwave.trailends.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.cloudwave.trailends.domain.UserRegInfo;
import com.cloudwave.trailends.mapper.UserStatMapper;
import com.cloudwave.trailends.service.UserStatService;

/**
 * @author 龙雪
 * @email 569141948@qq.com
 * @date 2015-2-10 下午8:11:14
 * 
 */

@Service
public class UserStatServiceImpl implements UserStatService {

	@Resource
	private UserStatMapper userStatMapper;
	
	
	@Override
	public void saveUserRegInfo(UserRegInfo regInfo) {
		userStatMapper.insertUserRegInfo(regInfo);
	}

}
