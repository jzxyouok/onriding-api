package com.cloudwave.trailends.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.cloudwave.base.entity.QueryEntity;
import com.cloudwave.trailends.domain.LinePoint;
import com.cloudwave.trailends.mapper.LinePointMapper;
import com.cloudwave.trailends.mapper.RidingLineMapper;
import com.cloudwave.trailends.service.LinePointService;

/**
 * @description 
 * @author wangwenlong
 * @date 2014年7月26日
 * TODO
 */

@Service
public class LinePointServiceImpl implements LinePointService {

	@Resource
	private LinePointMapper linePointMapper;
	@Resource
	private RidingLineMapper ridingLineMapper;
	
	@Override
	public void save(LinePoint lp) {
		linePointMapper.insert(lp);
	}
	
	@Override
	public void saveAll(List<LinePoint> lpList) {
		linePointMapper.insertAll(lpList);
	}

	@Override
	public List<LinePoint> findNotInIds(String lpIds) {
		return linePointMapper.findNotInIds(lpIds);
	}

	@Override
	public Long countByLastTime(Long userId, String rlIdStr, String lastTimestamp) {
		if ( StringUtils.isNotEmpty(rlIdStr) ) {
			long count1 = linePointMapper.countByLineId(userId, Long.parseLong(rlIdStr), lastTimestamp);
			long count2 = linePointMapper.countByOtherLine(userId, Long.parseLong(rlIdStr));
			return count1 + count2;
		} else {
			return linePointMapper.countByLastTime(userId, null);
		}
	}

	@Override
	public List<LinePoint> findBy(QueryEntity query) {
		
		return linePointMapper.findBy(query);
	}

	

}
