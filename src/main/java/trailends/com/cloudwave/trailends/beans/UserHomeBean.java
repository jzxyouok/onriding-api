package com.cloudwave.trailends.beans;


/**
 * @description 用户中心的数据模版
 * @author DolphinBoy
 * @email dolphinboyo@gmail.com
 * @date 2013-12-21 下午11:42:46
 * TODO
 */

public class UserHomeBean extends BaseBean {
	private static final long serialVersionUID = 1L;
	
	private long userId;
	private String userName;
	private String userAvatar;
	private int trips;  //旅程
	private long tripRecords;  //行记
	private long mileage;  //里程
	private int favorites;  //收藏
	private int likes;  //喜欢
	private int wish;  //想去
	private int follows;  //关注
	private int fans;  //粉丝
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserAvatar() {
		return userAvatar;
	}
	public void setUserAvatar(String userAvatar) {
		this.userAvatar = userAvatar;
	}
	public int getTrips() {
		return trips;
	}
	public void setTrips(int trips) {
		this.trips = trips;
	}
	public long getTripRecords() {
		return tripRecords;
	}
	public void setTripRecords(long tripRecords) {
		this.tripRecords = tripRecords;
	}
	public long getMileage() {
		return mileage;
	}
	public void setMileage(long mileage) {
		this.mileage = mileage;
	}
	public int getFavorites() {
		return favorites;
	}
	public void setFavorites(int favorites) {
		this.favorites = favorites;
	}
	public int getLikes() {
		return likes;
	}
	public void setLikes(int likes) {
		this.likes = likes;
	}
	public int getWish() {
		return wish;
	}
	public void setWish(int wish) {
		this.wish = wish;
	}
	public int getFollows() {
		return follows;
	}
	public void setFollows(int follows) {
		this.follows = follows;
	}
	public int getFans() {
		return fans;
	}
	public void setFans(int fans) {
		this.fans = fans;
	}
	
}