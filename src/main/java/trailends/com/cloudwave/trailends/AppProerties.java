package com.cloudwave.trailends;

/**
 * @description 系统配置
 * @author 龙雪
 * @email 3kqing@gmail.com
 * @date 2014-8-9 下午4:20:03
 * 
 */

public class AppProerties {

	public static final String HOST = "http://onriding.cc/";
	public static final String HOST_STATIC = "http://static.onriding.cc/";
	public static final String PUBLIC_UPLOAD_URL = HOST + "/uploads/";
	
	public static final String DEFAULT_AVATAR_URL = HOST + "/uploads/avatar/avatar_large.jpg";
	
	/**头像上传路径*/
	public static final String UPLOAD_AVATAR_BASE_PATH = "/alidata/www/default/uploads/avatar/";
//	public static final String UPLOAD_AVATAR_BASE_PATH = "E:\\data\\avatar\\";
	
	public static final String PATH_IMAGE = "http://onriding.cc/uploads/image/";
	public static final String PATH_AVATAR = "http://onriding.cc/uploads/avatar/";
}
