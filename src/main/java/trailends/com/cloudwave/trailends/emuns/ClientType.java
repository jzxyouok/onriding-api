package com.cloudwave.trailends.emuns;

/**
 * @description 客户端类型
 * @author DolphinBoy
 * @date 2013年12月18日 下午5:08:34 
 */

public enum ClientType {
	BROWSER("PC浏览器", "browser", 1), MOBILE("移动客户端", "mobile", 2);

	private String code;
	private String name;
	private int id;

	private ClientType(String name, String code, int id) {
		this.name = name;
		this.id = id;
		this.code = code;
	}

	public static ClientType getById(int v) {
		ClientType gender = null;
		ClientType[] list = ClientType.values();

		for (ClientType g : list) {
			if (g.id == v) {
				gender = g;
			}
		}
		return gender;
	}

	public static ClientType getByName(String name) {
		return ClientType.valueOf(name);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
