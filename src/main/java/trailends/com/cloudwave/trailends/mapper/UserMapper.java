package com.cloudwave.trailends.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.cloudwave.trailends.beans.UserHomeBean;
import com.cloudwave.trailends.domain.User;

/**
 * @description 
 * @author DolphinBoy
 * @email dolphinboyo@gmail.com
 * @date 2013-8-8 下午9:43:49
 * TODO
 */

public interface UserMapper {

	public User get(Long id);

	public void insert(User u);

	/**
	 * 根据邮箱查询用户
	 * @author wangwl
	 * @date 2013-8-22
	 * @param email
	 * @return
	 */
	public int countByEmail(@Param("email") String email);

	/**
	 * 用过账号查询用户
	 * @param username
	 * @return
	 */
	public int countByUsername(@Param("username") String username);
	
	/**
	 * 根据用户名和邮箱查询用户
	 * 这里在业务上来讲不能返回多条数据，但考虑到尽量少的抛出异常此处是否要用集合来接受数据呢？
	 * @param accountOrEmail
	 * @return
	 */
	public User findByAccountOrEmail(@Param("accountOrEmail") String accountOrEmail);

	public List<User> findByIds(List<Long> ids);

	public UserHomeBean loadUserHome(@Param("id") Long id);

	public User getByEmail(@Param("email") String email);

	public int countByNameAndEmail(@Param("username") String username, @Param("email") String email);

	public User findByToken(@Param("token") String token);

	public int countByAccount(@Param("account") String account);

	public User findByUserName(@Param("username") String username);

	public void updateAvatar(@Param("userId") Long userId, @Param("url") String url);

	public void updateToken(@Param("userId") Long userId, @Param("token") String token);

	public User findByOpenId(@Param("openid") String openid);

	public void updateBaseInfo(User user);

	public Long findIdByToken(@Param("token") String token);

}
