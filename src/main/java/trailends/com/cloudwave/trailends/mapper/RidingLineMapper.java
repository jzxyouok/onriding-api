package com.cloudwave.trailends.mapper;

import java.util.List;
import java.util.Map;

import com.cloudwave.trailends.domain.RidingLine;

/**
 * @description 
 * @author wangwenlong
 * @date 2014年7月26日
 * TODO
 */

public interface RidingLineMapper {

	void insert(RidingLine ridingLine);

	List<RidingLine> findNotInIds(String rlIds);

	List<RidingLine> findBy(Map<String, Object> params);

	List<RidingLine> findAll(Map<String, Object> params);

	Map<String, Object> statByUserId(Long userId);

	RidingLine findById(Long id);

	void deleteByIds(Map<String, Object> params);

	RidingLine findByLpId(Long lpId);

	RidingLine findFirst();

	RidingLine findNextByTime(Long timestamp);

	void update(RidingLine rlTmp);

}
