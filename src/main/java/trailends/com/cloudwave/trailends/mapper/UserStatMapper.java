package com.cloudwave.trailends.mapper;

import com.cloudwave.trailends.domain.UserRegInfo;

/**
 * @author 龙雪
 * @email 569141948@qq.com
 * @date 2015-2-10 下午8:09:33
 * 
 */

public interface UserStatMapper {

	void insertUserRegInfo(UserRegInfo regInfo);

}
