package com.cloudwave.base.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
/**
 * Spring IOC上下文工具类
 * 
 * @author Ryan Shaw
 * 
 */

@Component
public class SpringUtil implements ApplicationContextAware {
	private static Logger logger = LoggerFactory.getLogger(SpringUtil.class);
	
	
    /**
     * 当前IOC
     */
    private static ApplicationContext applicationContext;
    
    /**
     * 设置当前上下文环境，此方法由spring自动装配
     */
    @Override
    public void setApplicationContext(ApplicationContext acCtx)
            throws BeansException {
    	SpringUtil.applicationContext = acCtx;
        logger.debug("setApplicationContext...");
    }
    
    /**
     * 从当前IOC获取bean
     * 这个方法很怪了，如果default-autowire="default"的话这个方法获取不到对象，如果改为default-autowire="byName"则Action不被Spring维护
     * @param id bean的id
     * @return
     */
    public static Object getObject(String id) {
        return applicationContext.getBean(id);
    }
    
    public static <T> Object getObject(Class<T> clazz) {
    	return applicationContext.getBean(clazz);
    }
}