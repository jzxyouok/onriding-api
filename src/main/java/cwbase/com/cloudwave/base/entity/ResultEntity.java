package com.cloudwave.base.entity;

import com.cloudwave.fwcore.entity.Result;

/**
 * @description 通用返回结果实体类
 * @author 龙雪
 * @email 3kqing@gmail.com
 * @date 2013-10-13 下午2:27:33
 * 
 */

public class ResultEntity implements Result {
	private static final long serialVersionUID = 1L;
	public int result = 0;
	public int code;
	public Object data;
	
	public ResultEntity success() {
		this.result = 1;
		return this;
	}
	
	public ResultEntity success(Object data) {
		this.result = 1;
		this.code = 1;
		this.data = data;
		return this;
	}
	
	public ResultEntity failure() {
		this.result = 0;
		return this;
	}
	
	public ResultEntity failure(int code) {
		this.result = 0;
		this.code = code;
		return this;
	}
	
	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
