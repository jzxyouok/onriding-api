package com.cloudwave.base.web.inteceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.cloudwave.base.action.AppAction;
import com.cloudwave.base.entity.ResultEntity;

/**
 * @description 处理 request response 注入
 * @author wangwenlong
 * @date 2014年7月28日 TODO
 * 参考:http://blog.csdn.net/liuwenbo0920/article/details/7283757
 */

public class BaseInterceptor extends HandlerInterceptorAdapter {
	
	@Override
	public boolean preHandle(HttpServletRequest request
			, HttpServletResponse response, Object handler) throws Exception {
		HandlerMethod hm = (HandlerMethod) handler;
		
		AppAction bc = (AppAction) hm.getBean();
		bc.setRequest(request);
		bc.setResponse(response);
		ResultEntity resultEntity = new ResultEntity();
		bc.setResultEntity(resultEntity);
		return super.preHandle(request, response, handler);
	}
}
