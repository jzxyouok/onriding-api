package com.cloudwave.base.action;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.cloudwave.base.domain.AppDomain;
import com.cloudwave.base.entity.ResultEntity;
import com.cloudwave.fwcore.utils.DateUtils;
import com.cloudwave.fwcore.utils.ImageUtil;
import com.cloudwave.trailends.AppProerties;
import com.cloudwave.trailends.domain.FileEntity;
import com.cloudwave.trailends.domain.User;
import com.cloudwave.trailends.domain.UserSignLog;



/**
 * @description 抽象业务Action
 * @author 龙雪
 * @email 3kqing@gmail.com
 * @date 
 * @TODO 定义了通用的抽象业务方法
 */
public class AppAction extends BaseAction {
	
	protected HttpServletRequest request; 
	protected HttpServletResponse response;
	protected String token;
	protected Long userId;
	protected ResultEntity resultEntity;
	
	protected UserSignLog signLog;
	protected String channelId;
	protected String udid;
	protected String imei;
	protected String phone;
	
	
	
	protected FileEntity saveFile(MultipartFile file) throws IllegalStateException, IOException {
		if (!file.isEmpty()) {
			FileEntity fe = new FileEntity();
			
            String contentType = file.getContentType();
            String type = file.getName().substring(file.getName().indexOf(".")+1, file.getName().length());
            
            String fileName = file.getOriginalFilename();
//            String path = "F:/Temp/";
            // 这里的命名规则应该是: 用户ID+年+随机文件名
//            String path = "F:/Temp/App.Cache/";
            String saveName = UUID.randomUUID()
                    + fileName.substring(fileName.lastIndexOf("."));
            String savePath = AppProerties.UPLOAD_AVATAR_BASE_PATH + saveName;
            
            fe.setName(fileName);
            fe.setSaveName(saveName);
            fe.setSavePath(savePath);
            fe.setContentType(contentType);
            fe.setType(type);
            fe.setUploadTime(new Date());
            fe.setMd5("");
            
            File newfile = new File(savePath);
            
            file.transferTo(newfile);
            
            return fe;
		} else {
			return null;
		}
	}
	
	
	protected String uploadFile(MultipartFile file, Integer userId) throws IllegalStateException, IOException {
		if (!file.isEmpty()) {
			
            String type = file.getName().substring(file.getName().lastIndexOf("."), file.getName().length());
//            String fileName = file.getOriginalFilename();
//            String path = "F:/Temp/";
            // 这里的命名规则应该是: 用户ID+年+随机文件名
//            String path = "F:/Temp/App.Cache/";
            String saveName = DateUtils.format(DateUtils.now(), "yyyyMM")+"/"+DateUtils.format(DateUtils.now(), DateUtils.yearMonthDayHHMMssSSS)+userId+type;
            String savePath = AppProerties.UPLOAD_AVATAR_BASE_PATH + saveName;
            
            File newfile = new File(savePath);
            
            file.transferTo(newfile);
            
            String imageURL = AppProerties.PATH_AVATAR + saveName;
            
            return imageURL;
		} else {
			return null;
		}
	}

	protected String uploadAvatar(MultipartFile file, Long userId) throws IllegalStateException, IOException {
		if (!file.isEmpty()) {
			
			//路径的生成要和这个一样Upload_m
			String fileName = file.getOriginalFilename();
			String type = ".png";
			if ( fileName.indexOf(".") > -1 ) {
				String typeTmp = fileName.substring(fileName.lastIndexOf("."), fileName.length());
				if ( StringUtils.isNotEmpty(typeTmp) ) {
					type = typeTmp;
				}
            }

			//这里要检测文件类型
			//通过file.getContentType()检测或者通过type检测
			
//            String fileName = file.getOriginalFilename();
//            String path = "F:/Temp/";
            // 这里的命名规则应该是: 用户ID+年+随机文件名
//            String path = "F:/Temp/App.Cache/";
            String saveName = userId+"/"+userId+"_avatar_original"+type;
            String savePath = AppProerties.UPLOAD_AVATAR_BASE_PATH + saveName;
            
            File newfile = new File(savePath);
            if ( !newfile.exists() ) {
            	newfile.mkdirs();
            }
            file.transferTo(newfile);
            
            String saveBigName = userId+"/"+userId+"_avatar_big"+type;
            String saveMobileName = userId+"/"+userId+"_avatar_mobile"+type;
            String saveMiddleName = userId+"/"+userId+"_avatar_middle"+type;
            String saveSmallName = userId+"/"+userId+"_avatar_small"+type;
            
            //###这个地方比较耗时,考虑放到线程里面去
            ImageUtil.compressImage(savePath, AppProerties.UPLOAD_AVATAR_BASE_PATH + saveBigName, 100, 100);
            ImageUtil.compressImage(savePath, AppProerties.UPLOAD_AVATAR_BASE_PATH + saveMobileName, 80, 80);
            ImageUtil.compressImage(savePath, AppProerties.UPLOAD_AVATAR_BASE_PATH + saveMiddleName, 48, 48);
            ImageUtil.compressImage(savePath, AppProerties.UPLOAD_AVATAR_BASE_PATH + saveSmallName, 24, 24);
            
            
            String imageURL = AppProerties.PATH_AVATAR + saveMiddleName;
            
            return imageURL;
		} else {
			return null;
		}
	}
	
	protected void setWho(AppDomain domain) {
		User createBy = new User(userId);
		domain.setCreateBy(createBy);
		domain.setCreatorId(userId);
	}
	protected void setWho(AppDomain domain, Date time) {
		User createBy = new User(userId);
		domain.setCreateBy(createBy);
		domain.setCreatorId(userId);
//		domain.setCreateTime(time);
		domain.setLastUpdateTime(time);
	}
	
	protected void setWho(List<? extends AppDomain> domainList) {
		User createBy = new User(userId);
		for (AppDomain domain : domainList) {
			domain.setCreatorId(userId);
			domain.setCreateBy(createBy);
		}
	}
	protected void setWho(List<? extends AppDomain> domainList, Date time) {
		User createBy = new User(userId);
		for (AppDomain domain : domainList) {
			domain.setCreateBy(createBy);
			domain.setCreatorId(userId);
//			domain.setCreateTime(time);
			domain.setLastUpdateTime(time);
		}
	}
	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public ResultEntity getResultEntity() {
		return resultEntity;
	}

	public void setResultEntity(ResultEntity resultEntity) {
		this.resultEntity = resultEntity;
	}
	
	public UserSignLog getUserSignLog() {
		return this.signLog;
	}
	
	public void setUserSignLog(UserSignLog signLog) {
		this.signLog = signLog;
	}
}
