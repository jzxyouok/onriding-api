package com.cloudwave.base.domain;

import java.util.Date;

import com.cloudwave.trailends.domain.User;

/**
 * @description 业务实体类, 主要定义通用的业务属性
 * @author DolphinBoy
 * 2013年12月6日 上午11:17:39
 * 
 */

public class AppDomain extends BaseEntity {
	private static final long serialVersionUID = -7313156613433703333L;
	
	protected Long remoteId;
	protected Date createTime;
	protected User createBy;
	protected Long creatorId;
	protected Long timestamp;
	protected Date sendTime;
	protected Date lastUpdateTime;
	protected int sendStatus = 1;
	
	public Long getRemoteId() {
		return remoteId;
	}
	public void setRemoteId(Long remoteId) {
		this.remoteId = remoteId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public User getCreateBy() {
		return createBy;
	}
	public void setCreateBy(User createBy) {
		this.createBy = createBy;
	}
	public Date getSendTime() {
		return sendTime;
	}
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	public int getSendStatus() {
		return sendStatus;
	}
	public void setSendStatus(int sendStatus) {
		this.sendStatus = sendStatus;
	}
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public Long getCreatorId() {
		return creatorId;
	}
	public void setCreatorId(Long creatorId) {
		this.creatorId = creatorId;
	}
	public Long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}
	
}
